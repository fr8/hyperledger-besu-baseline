# Besu Baseline 

#### Running Besu permissioned network to ethereum main net

Compile contracts, build project, and install Hyperledger Besu:

`gradle clean build installBesu`

#### Start Hyperledger Besu:

`build/besu-1.4.5/bin/besu --config-file=config.toml`

Wait until you see that you are connected with the Freight Trust Network global chain

From another terminal go to the directory, compile and start:

`gradle run`


### License 

Apache-2.0

### Security
admin@freighttrust.com
